# dlpopsize: Deep Learning for Population Size History Inference


This repository is based on the paper ["Deep learning for population size history inference: design, comparison and combination with approximate Bayesian computation"](https://www.biorxiv.org/content/10.1101/2020.01.20.910539v1.full.pdf) (Sanchez et al.) and contains the following items:

- a [notebook](example.ipynb) to illustrate the prediction of population size using a pre-trained network on some examples
- a [file](dl/net.py) giving the pytorch implementation of our SPIDNA architecture with the lowest prediction error (see the [paper](https://www.biorxiv.org/content/10.1101/2020.01.20.910539v1.full.pdf) for details)
- a [directory](pretrained_SPIDNA/) containing the pretrained SPIDNA network has a .pth file and another .pth compatible with the package [dnadna](https://gitlab.com/mlgenetics/dnadna/)

The file [dl/net.py](dl/net.py) also contains a version of SPIDNA adaptive to input size for information purpose only. 

Data and code used for ["Deep learning for population size history inference: design, comparison and combination with approximate Bayesian computation"](https://www.biorxiv.org/content/10.1101/2020.01.20.910539v1.full.pdf) (Sanchez et al.) are available on the ["dlpopsize\_paper"](https://gitlab.inria.fr/ml_genetics/public/dlpopsize_paper) repository.
